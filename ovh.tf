resource "ovh_cloud_project_kube" "primary" {
  region       = "GRA5"
  name         = "gitlab-terraform-OVH"
  version      = "1.24"
  service_name = "your_service_name"
}

resource "ovh_cloud_project_kube_nodepool" "default" {
  kube_id        = ovh_cloud_project_kube.primary.id
  name           = "default"
  flavor_name    = "d2-4"
  desired_nodes  = 3
  max_nodes      = 3
  min_nodes      = 3
  monthly_billed = false
  service_name   = "your_service_name"
}
