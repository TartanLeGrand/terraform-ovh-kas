terraform {
  required_providers {
    ovh = {
      source = "ovh/ovh"
    }
    helm = {
      source = "hashicorp/helm"
    }
  }
}
