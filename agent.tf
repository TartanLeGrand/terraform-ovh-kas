resource "helm_release" "gitlab_agent" {
  name = "gitlab-agent"

  repository = "https://charts.gitlab.io"
  chart      = "gitlab-agent"

  create_namespace = true
  namespace        = "gitlab-agent"

  set {
    name  = "config.kasAddress"
    value = "wss://kas.gitlab.com"
  }

  set {
    name  = "config.token"
    value = "your-token"
  }
}
